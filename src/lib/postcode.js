const http = require('https');

const priceByPostcode = (_postcode) => {
  const postcode = _postcode.replace(' ', '');
  const requestUrl = `https://api.postcodes.io/postcodes/${postcode}`;

  return new Promise((resolve, reject) => {
    http.get(requestUrl, (res) => {
      const statusCode = res.statusCode;
      res.setEncoding('utf8');
      responseData(res).then((text) => {
        const postcodeResult = JSON.parse(text).result;
        getPrice(postcodeResult.msoa).then((price) => {
          resolve({
            postcode: postcodeResult,
            price,
          });
        }, reject);
      }, reject);
    });
  });
};

const getPrice = msoa =>
  new Promise((resolve, reject) => {
    const formatted = msoa.split(' ').join('');
    const url = `https://www.ons.gov.uk/visualisations/dvc393/affordabilitycalculator/data/Cut2/${formatted}.csv`;
    http.get(url, (res) => {
      responseData(res).then((text) => {
        if (text) {
          const [keys, values] = text.split('\n');
          const result = values.split(',').reduce((acc, val, index) => {
            acc[keys.split(',')[index]] = val;
            return acc;
          }, {});
          const yearlyIncome = parseFloat(result['Income_Est\r']) * 52.12;
          result.Income_Est = yearlyIncome;
          result.Price_To_Income = Number(result.House_Price / yearlyIncome).toFixed(2);
          resolve(result);
        } else {
          reject({
            status: 404,
            error: 'Price not found',
          });
        }
      });
    });
  });

const responseData = res =>
  new Promise((resolve, reject) => {
    let rawData = '';
    res.on('data', chunk => (rawData += chunk));
    res.on('end', () => {
      res.statusCode === 200 ? resolve(rawData) : reject(rawData);
    });
  });

// module.exports.priceByPostcode = priceByPostcode;

module.exports.priceByPostcode = () => {
  const csv = require('fs')
    .readFileSync('./data/ppd.csv', 'utf-8')
    .toString();

  const transactions = csv.split('\n').reduce((acc, line) => {
    let [
      id,
      price,
      date,
      postcode,
      type,
      newBuild,
      estateType,
      building,
      town,
      district,
      county,
      borough,
    ] = line.split(',');

    if (type) {
      type = type.replace(/\"/g, '');
      newBuild = newBuild.replace(/\"/g, '');

      const transaction = {
        id,
        price: parseFloat(price.replace(/\"/g, '')),
        date: date.replace(/\"/g, ''),
        postcode: postcode.replace(/\"/g, ''),
        type,
        newBuild: newBuild.replace(/\"/g, ''),
        estateType: estateType.replace(/\"/g, ''),
        building: building.replace(/\"/g, ''),
        town: town.replace(/\"/g, ''),
        county: county.replace(/\"/g, ''),
      };

      acc[type] = acc[type] || [];
      acc[type].push(transaction);

      acc[type + newBuild] = acc[type + newBuild] || [];
      acc[type + newBuild].push(transaction);
    }

    return acc;
  }, {});

  const avgTransactions = Object.keys(transactions).map((key) => {
    const typeTransactions = transactions[key];
    const avg = typeTransactions.reduce(
      (acc, transac) => acc + Math.floor(transac.price / typeTransactions.length),
      0,
    );
    return {
      key,
      avg,
    };
  });

  return Promise.resolve(avgTransactions);
};
