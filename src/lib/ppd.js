
let csvToTransactions = function (csv) {

    return csv.split('\n').reduce((acc, line) => {
        let [id,
            price,
            date,
            postcode,
            type,
            newBuild,
            estateType,
            building,
            town,
            district,
            county,
            borough
        ] = line.split(',')

        if (type) {

            type = type.replace(/\"/g, '')
            newBuild = newBuild.replace(/\"/g, '')

            let transaction = {
                id,
                price: parseFloat(price.replace(/\"/g, '')),
                date: date.replace(/\"/g, ''),
                postcode: postcode.replace(/\"/g, ''),
                type,
                newBuild: newBuild.replace(/\"/g, ''),
                estateType: estateType.replace(/\"/g, ''),
                building: building.replace(/\"/g, ''),
                town: town.replace(/\"/g, ''),
                county: county.replace(/\"/g, '')
            }

            // basic types including new builds
            acc[type] = acc[type] || []
            acc[type].push(transaction)

            // types of new builds
            acc[type + newBuild] = acc[type + newBuild] || []
            acc[type + newBuild].push(transaction)
        }

        return acc
    }, {})
}

let calculateAvgerages = function (transactions) {

    return Object.keys(transactions).map((key) => {
        let typeTransactions = transactions[key]
        let avg = typeTransactions.reduce((acc, transac) => {
            return acc + Math.floor(transac.price / typeTransactions.length)
        }, 0)
        return {
            key,
            avg
        }
    })

}

let createParams = function (postcode) {
    let params = {
        postcode,
        min_date: '1+January+2017',
        limit: 'all',
        'et[]': [
            'lrcommon:freehold',
            'lrcommon:leasehold'
        ],
        'ptype[]': [
            'lrcommon:detached',
            'lrcommon:semi-detached',
            'lrcommon:terraced',
            'lrcommon:flat-maisonette'
        ],
        'tc[]': [
            'ppd:standardPricePaidTransaction',
            'ppd:additionalPricePaidTransaction'
        ]
    }

    return Object.keys(params).reduce((acc, key) => {
        let value = params[key]
        if (Array.isArray(value)) {
            return acc.concat(value.map(v => `${key}=${v}`))
        } else {
            return acc.concat([`${key}=${value}`])
        }
    }, [])
}

const responseData = (res) => {
    return new Promise((resolve, reject) => {
        let rawData = '';
        res.on('data', (chunk) => rawData += chunk);
        res.on('end', () => {
            res.statusCode === 200 ? resolve(rawData) : reject(rawData)
        });
    });
}


let byPostcode = function (postcode) {
    const http = require('http')

    const uri = `http://landregistry.data.gov.uk/app/ppd/ppd_data.csv?et%5B%5D=lrcommon%3Afreehold&et%5B%5D=lrcommon%3Aleasehold&limit=all&min_date=1+December+2016&nb%5B%5D=true&nb%5B%5D=false&postcode=${postcode}&ptype%5B%5D=lrcommon%3Adetached&ptype%5B%5D=lrcommon%3Asemi-detached&ptype%5B%5D=lrcommon%3Aterraced&ptype%5B%5D=lrcommon%3Aflat-maisonette&ptype%5B%5D=lrcommon%3AotherPropertyType&tc%5B%5D=ppd%3AstandardPricePaidTransaction&tc%5B%5D=ppd%3AadditionalPricePaidTransaction`
    return new Promise((resolve, reject) => {
        http.get(uri, (res) => {
            responseData(res).then((text) => {
                const transactions = csvToTransactions(text)
                const avgs = calculateAvgerages(transactions)
                resolve(avgs)
            }, reject);
        })
    })
}

module.exports = byPostcode
