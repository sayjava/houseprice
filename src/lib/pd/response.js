module.exports.responseData = (res) => {
    return new Promise((resolve, reject) => {
        let rawData = '';
        res.on('data', (chunk) => rawData += chunk);
        res.on('end', () => {
            res.statusCode === 200 ? resolve(rawData) : reject(rawData)
        });
    });
}