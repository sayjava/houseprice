let csvToTransactions = function (csv) {

    return csv.split('\n').reduce((acc, line) => {
        let [id,
            price,
            date,
            postcode,
            type,
            newBuild,
            estateType,
            building,
            town,
            district,
            county,
            borough
        ] = line.split(',')

        if (type) {

            type = type.replace(/\"/g, '')
            newBuild = newBuild.replace(/\"/g, '')

            let transaction = {
                id,
                price: parseFloat(price.replace(/\"/g, '')),
                date: date.replace(/\"/g, ''),
                postcode: postcode.replace(/\"/g, ''),
                type,
                newBuild: newBuild.replace(/\"/g, ''),
                estateType: estateType.replace(/\"/g, ''),
                building: building.replace(/\"/g, ''),
                town: town.replace(/\"/g, ''),
                county: county.replace(/\"/g, '')
            }

            // basic types including new builds
            acc[type] = acc[type] || []
            acc[type].push(transaction)

            // types of new builds
            acc[type + newBuild] = acc[type + newBuild] || []
            acc[type + newBuild].push(transaction)
        }

        return acc
    }, {})
}

let calculateAvgerages = function (transactions) {

    return Object.keys(transactions).map((key) => {
        let typeTransactions = transactions[key]
        let avg = typeTransactions.reduce((acc, transac) => {
            return acc + Math.floor(transac.price / typeTransactions.length)
        }, 0)
        return {
            key,
            avg
        }
    })

}

module.exports.averages = calculateAvgerages
module.exports.transactions = csvToTransactions