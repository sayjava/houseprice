const responseData = require('./response').responseData
const csv = require('./csv')

module.exports.byPostcode = function (postcode) {
    const http = require('http')
    const uri = `http://landregistry.data.gov.uk/app/ppd/ppd_data.csv?et%5B%5D=lrcommon%3Afreehold&et%5B%5D=lrcommon%3Aleasehold&limit=all&min_date=1+December+2016&nb%5B%5D=true&nb%5B%5D=false&postcode=${postcode}&ptype%5B%5D=lrcommon%3Adetached&ptype%5B%5D=lrcommon%3Asemi-detached&ptype%5B%5D=lrcommon%3Aterraced&ptype%5B%5D=lrcommon%3Aflat-maisonette&ptype%5B%5D=lrcommon%3AotherPropertyType&tc%5B%5D=ppd%3AstandardPricePaidTransaction&tc%5B%5D=ppd%3AadditionalPricePaidTransaction`

    return new Promise((resolve, reject) => {
        http.get(uri, (res) => {
            responseData(res).then((text) => {
                const transactions = csv.transactions(text)
                const avgs = csv.averages(transactions)
                resolve(avgs)
            }, reject);
        })
    })
}