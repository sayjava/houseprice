
const TAXES = [
    { lower: 125000, tax: 0, threshold: 125000 },
    { lower: 125000, threshold: 250000, tax: 2 },
    { lower: 250000, threshold: 925000, tax: 5 },
    { lower: 925000, threshold: 1500000, tax: 10 },
    { lower: 1500000, threshold: 100000000000000, tax: 12 }
]

const FEES = {
    VALUATION: [150, 1500],
    SURVEY: [250, 600],
    LEGAL: [1020, 1800],
    LOCAL_SEARCH: [250, 300],
    MORTGAGE_FEE: [99, 250],
    MORTGAGE_VALUATION: [150, 150],
    MORTGAGE_ARRANGEMENT: [750, 2000]
}

const stampDuty = (housePrice = 0) => {

    let bands = TAXES.filter(band => housePrice >= band.lower + 1)

    let calc = bands.reduce((calc, band) => {

        if (calc.taxable > band.lower) {
            calc.taxable -= band.lower
            calc.tax += band.lower * (band.tax / 100)
        } else {
            calc.tax += calc.taxable * (band.tax / 100)
            calc.taxable = 0;
        }

        return calc
    }, { tax: 0, taxable: housePrice })


    return calc.tax;
}

module.exports.fees = FEES

module.exports.stampDuty = stampDuty

module.exports.totalCost = ({ housePrice, deposit }) => {

    let downPayment = housePrice * (deposit / 100)
    let duty = stampDuty(housePrice)
    let baseCost = duty + downPayment

    let lowerCost = Math.round(Object.keys(FEES).reduce((total, feeType) => {
        let [lowerFee] = FEES[feeType]
        return total + lowerFee
    }, baseCost))

    let higherCost = Math.round(Object.keys(FEES).reduce((total, feeType) => {
        let [, higherFee] = FEES[feeType]
        return total + higherFee
    }, baseCost))

    return {
        lower: lowerCost,
        higher: higherCost
    }
}