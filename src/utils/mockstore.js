export default state => ({
  getState: () => state,
  subscribe: jest.fn(),
  dispatch: jest.fn(),
});
