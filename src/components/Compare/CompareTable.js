import React from 'react';
import PropTypes from 'proptypes';
import { connect } from 'react-redux';
import { clearPriceHistory } from '../../services/history';

import './table.css';

const stateToProps = ({ history }) => history;
const actionsToFunctions = {
  clearPrice: clearPriceHistory,
};

const Table = (props) => {
  const { priceHistory, clearPrice } = props;

  const formatPrice = (entry, type) => {
    const price = entry.prices[type] ? entry.prices[type].price : '';
    const [address] = entry.address.text.split(',');
    return {
      address,
      price,
    };
  };

  const renderHouseType = type =>
    Object.keys(priceHistory).map(key => (
      <div
        key={key}
        data-address={formatPrice(priceHistory[key], type).address}
        className="addresses-price__block"
      >
        <i role="presentation" className="fa fa-trash-o" onClick={() => clearPrice(key)} />
        {formatPrice(priceHistory[key], type).price}
      </div>
    ));

  return (
    <div className="price-table-container">
      <div className="price-table">
        <div className="addresses">
          <div className="addresses-address" />
          {Object.keys(priceHistory).map((key) => {
            const flat = formatPrice(priceHistory[key], 'FN');
            return (
              <div key={`Address-${key}`} className="addresses-address">
                <i role="presentation" className="fa fa-trash-o" onClick={() => clearPrice(key)} />
                {flat.address}
              </div>
            );
          })}
        </div>
        <div className="prices">
          <div className="prices-row">
            <div className="addresses-type__block">Flats</div>
            {renderHouseType('FN')}
          </div>
          <div className="prices-row">
            <div className="addresses-type__block">Terraced</div>
            {renderHouseType('TN')}
          </div>
          <div className="prices-row">
            <div className="addresses-type__block">Semi-Detached</div>
            {renderHouseType('SN')}
          </div>
          <div className="prices-row">
            <div className="addresses-type__block">Detached</div>
            {renderHouseType('DN')}
          </div>
        </div>
      </div>
    </div>
  );
};

export default connect(stateToProps, actionsToFunctions)((props) => {
  const { priceHistory } = props;
  const emptyHistory = Object.keys(priceHistory).length === 0;

  const view = !emptyHistory ? (
    <Table {...props} />
  ) : (
    <div className="empty-price-table">
      <i className="fa fa-search uk-text-primary" />
      Search for prices by postcodes or districts to compare average prices
    </div>
  );

  return view;
});

Table.propTypes = {
  priceHistory: PropTypes.objectOf(Object).isRequired,
  clearPrice: PropTypes.func.isRequired,
};
