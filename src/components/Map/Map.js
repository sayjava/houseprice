import React from 'react';
import PropTypes from 'proptypes';
import { connect } from 'react-redux';

import './map.css';

export class Map extends React.Component {
  static centerMap(map, prices) {
    const bound = new window.google.maps.LatLngBounds();
    Object.keys(prices).forEach((id) => {
      const { location } = prices[id].address.geometry;
      bound.extend(location);
    });
    map.fitBounds(bound);
  }

  shouldComponentUpdate(props) {
    this.createMarkers(props.priceHistory);
    Map.centerMap(this.map, props.priceHistory);
    return false;
  }

  clearMap() {
    (this.markers || []).forEach(marker => marker.setMap(null));
  }

  createMarkers(history) {
    this.clearMap();
    this.markers = Object.keys(history).map((id) => {
      const price = history[id];
      const { location } = price.address.geometry;
      return new window.google.maps.Marker({
        map: this.map,
        position: location,
      });
    });
  }

  renderMap(domNode) {
    this.map = new window.google.maps.Map(domNode, {
      center: { lat: 51.4835, lng: 0.0 },
      zoom: 15,
      backgroundColor: '#ffffff',
      draggable: true,
      clickableIcons: false,
      disableDefaultUI: false,
      disableDoubleClickZoom: true,
      fullscreenControl: false,
      gestureHandling: 'none',
    });

    if (this.props.priceHistory) {
      this.createMarkers(this.props.priceHistory);
      Map.centerMap(this.map, this.props.priceHistory || {});
    }
  }

  render() {
    return <div className="map-container" ref={dom => this.renderMap(dom)} />;
  }
}

Map.propTypes = {
  priceHistory: PropTypes.objectOf(Object).isRequired,
};

export default connect(({ history }) => history, null)(Map);
