import PropTypes from 'proptypes';
import React, { Component } from 'react';

const debounce = function debounce(func, wait, immediate) {
  let timeout;
  return function (...rest) {
    const context = this;
    const args = rest;
    const later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

export default class PostcodeInput extends Component {
  constructor(props) {
    super(props);
    const { onTextInput, clearInput } = this.props;
    this.textInput = debounce((input) => {
      if (input.value.length > 0) {
        onTextInput(input.value);
      } else {
        clearInput();
      }
    }, 500);
  }

  //   nodeReady(inputForm) {
  //     debounce(() => {}, 500);

  //     Observable.fromEvent(inputForm, 'input')
  //       .debounce(500)
  //       .subscribe((evt) => {
  //         if (evt.srcElement.value.length > 0) {
  //           this.props.onTextInput(evt.srcElement.value);
  //         } else {
  //           this.props.clearInput();
  //         }
  //       });
  //   }

  render() {
    const { clearInput } = this.props;
    return (
      <form className="address-search__form" onSubmit={e => e.preventDefault()}>
        <input
          className="address-search__input"
          type="text"
          onChange={e => this.textInput(e.target)}
          placeholder="E.g SW16, Plymouth, Devon"
          onBlur={() => clearInput()}
        />
      </form>
    );
  }
}

PostcodeInput.propTypes = {
  clearInput: PropTypes.func.isRequired,
  onTextInput: PropTypes.func.isRequired,
};
