import React from 'react';
import PropTypes from 'proptypes';

const AddressList = ({ addresses, onAddressSelect }) => (
  <ul className="address-list">
    {addresses.map(address => (
      <li
        role="presentation"
        key={address.id}
        className="address-list__address"
        onClick={() => onAddressSelect(address)}
      >
        <span>{address.description}</span>
      </li>
    ))}
  </ul>
);

AddressList.propTypes = {
  addresses: PropTypes.arrayOf(Object).isRequired,
  onAddressSelect: PropTypes.func.isRequired,
};

export default AddressList;
