import React from 'react';
import { connect } from 'react-redux';

import { searchAddress, selectAddress, clearAddress } from '../../services/address';
import { fetchPrices, clearPrice } from '../../services/price';
import { fetchPostcode } from '../../services/postcode';
import { newHistory } from '../../services/history';

import AddressInput from './AddressInput';
import AddressList from './AddressList';

import './AddressInput.css';

const stateToProps = ({ address, price: { fetchingProgress }, postcode: { postcode } }) => ({
  fetchingProgress,
  addresses: address.addresses || [],
  selectedAddress: address.selectedAddress,
  postcode,
});

export default connect(stateToProps, {
  onTextInput: searchAddress,
  clearInput: () => (dispatch) => {
    setTimeout(() => dispatch(clearAddress()), 500);
  },
  onAddressSelect: (address) => {
    // scroll prices into view
    if (document.querySelector('.prices')) {
      document.querySelector('.prices').scrollIntoView();
    }

    return (dispatch) => {
      dispatch(selectAddress(address));
      dispatch(fetchPostcode(address))
        .then(postcode => dispatch(fetchPrices(postcode)))
        .then(pricesResult => Promise.resolve(dispatch(newHistory(pricesResult))))
        .then(() => {
          dispatch(clearAddress());
          dispatch(clearPrice());
        });
    };
  },
})((props) => {
  const { selectedAddress, postcode } = props;
  if (!props.fetchingProgress) {
    return (
      <div className="address-input">
        <span className="uk-text-meta fa fa-search address-input__search-icon" />
        <AddressInput {...props} />
        {!selectedAddress ? <AddressList {...props} /> : null}
      </div>
    );
  }
  return (
    <div className="price-content__progress">
      Crunching the numbers for {postcode.text}
      <i className="fa fa-cog fa-spin fa-1x fa-fw" />
    </div>
  );
});
