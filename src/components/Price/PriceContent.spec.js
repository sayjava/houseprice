import { mount } from 'enzyme';
import React from 'react';
import PriceContent from './PriceContent';
import mockStore from '../../utils/mockstore';

describe('Price Content', () => {
  describe('Error', () => {
    it('should render an error view if an error is present in the price state', () => {
      const store = mockStore({
        price: {
          error: { message: 'Failed' },
          address: { name: 'westminster' },
        },
      });
      const component = mount(<PriceContent store={store} />);
      expect(component.find('.price-content__error').text()).toEqual(' Failed for westminster');
    });

    it('should not render an error view if error is null', () => {
      const store = mockStore({
        price: {
          error: null,
          address: { name: 'westminster' },
        },
      });
      const component = mount(<PriceContent store={store} />);
      expect(component.find('.price-content__error').exists()).toBe(false);
    });
  });

  describe('Progress', () => {
    it('should render a progress view the address name', () => {
      const store = mockStore({
        price: {
          fetchingProgress: {},
          address: { name: 'westminster' },
        },
      });
      const component = mount(<PriceContent store={store} />);
      expect(component.find('.price-content__progress').text()).toBe(
        'Crunching the numbers for westminster',
      );
    });

    it('should render no errors whilst address fetch is in progress', () => {
      const store = mockStore({
        price: {
          error: {},
          address: { name: 'westminster' },
        },
      });
      const component = mount(<PriceContent store={store} />);
      expect(component.find('.price-content__progress').exists()).toBe(false);
    });
  });
});
