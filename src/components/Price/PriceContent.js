import React from 'react';
import { connect } from 'react-redux';

import './price.css';

const stateToProps = ({ price: { error, fetchingProgress, address } }) => ({
  fetchingProgress,
  error,
  address,
});

export default connect(stateToProps)(({ fetchingProgress, error, address }) => (
  <div className="price-content">
    {error ? (
      <div className="price-content__error">
        <i className="fa fa-exclamation-triangle" /> {error.message} for {address.name}
      </div>
    ) : null}
    {fetchingProgress ? (
      <div className="price-content__progress">
        Crunching the numbers for {address.name}
        <i className="fa fa-cog fa-spin fa-1x fa-fw" />
      </div>
    ) : null}
  </div>
));
