import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';

import { fillHistory } from './services/history';
import reducers from './services';
import App from './app';

import './vendor/styles/font-awesome.min.css';
import registerServiceWorker from './registerServiceWorker';

/* eslint no-underscore-dangle: */
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducers, composeEnhancers(applyMiddleware(thunk)));

// fill up the history
store.dispatch(fillHistory());

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
);
registerServiceWorker();
