import { combineReducers } from 'redux';
import address from './address';
import price from './price';
import history from './history';
import postcode from './postcode';

export default combineReducers({
  address,
  price,
  history,
  postcode,
});
