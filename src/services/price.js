import lscache from 'lscache';

const LOCAL_URL = 'http://localhost:8010/houseprice-7da29/us-central1';
const PROD_URL = 'https://us-central1-houseprice-7da29.cloudfunctions.net';
const isLocal = window.location.hostname === 'localhost';
const BASE_URL = isLocal ? LOCAL_URL : PROD_URL;
const ONE_MONTH = 34560;

const L10nGBP = new Intl.NumberFormat('en-GB', {
  style: 'currency',
  currency: 'GBP',
  minimumFractionDigits: 0,
});

const formatPrice = json =>
  json.reduce((acc, avg) => {
    const avgObj = { ...avg, price: L10nGBP.format(avg.avg) };
    acc[avg.key] = avgObj;
    return acc;
  }, {});

const FETCHING_PRICES = 'fetching_prices';
const FETCH_PRICES_SUCCESSFUL = 'fetch_prices_successful';
const FETCH_PRICES_FAILURE = 'fetch_prices_failure';
const CLEAR_PRICE = 'clear_price';

const priceSuccessful = (postcode, prices) => ({
  type: FETCH_PRICES_SUCCESSFUL,
  prices,
  postcode,
});

const priceFailed = error => ({ type: FETCH_PRICES_FAILURE, error });
const fetchingPrice = address => ({ type: FETCHING_PRICES, address });

export const clearPrice = () => ({ type: CLEAR_PRICE });

export const fetchPrices = (address) => {
  let fetchURL = '';

  if (address.postcode) {
    fetchURL = `${BASE_URL}/postcode?postcode=${address.postcode.short_name}`;
  } else if (address.district) {
    fetchURL = `${BASE_URL}/district?district=${address.district}`;
  } else {
    fetchURL = `${BASE_URL}/district?district=${address.name}`;
  }

  return (dispatch) => {
    const isPricesEmpty = prices => !(prices && Object.keys(prices).length > 0);

    const sendResult = (prices) => {
      if (isPricesEmpty(prices)) {
        dispatch(
          priceFailed({
            message: `Prices not found for ${address.name}, please try another area or postcode`,
          }),
        );
        window.ga('send', 'event', 'search', 'failed', address.name);
        return Promise.resolve({
          prices,
          address,
        });
      }
      dispatch(priceSuccessful(address, prices));
      window.ga('send', 'event', 'search', 'success', address.name);
      return Promise.resolve({ address, prices });
    };

    dispatch(fetchingPrice(address));

    if (lscache.get(fetchURL)) {
      const cachedPrices = lscache.get(fetchURL);
      return sendResult(cachedPrices);
    }
    return fetch(fetchURL)
      .then(result => result.json())
      .then((json) => {
        try {
          const prices = formatPrice(json);
          lscache.set(fetchURL, prices, ONE_MONTH);
          return sendResult(prices);
        } catch (e) {
          window.ga('send', 'event', 'search', 'failed', address.name);
          return dispatch(priceFailed(json));
        }
      })
      .catch(e => dispatch(priceFailed(e)));
  };
};

export default (state = { prices: null }, action) => {
  if (action.type === FETCH_PRICES_SUCCESSFUL) {
    return {
      ...state,
      prices: action.prices,
      fetchingProgress: false,
      error: null,
      postcode: action.postcode,
    };
  }

  if (action.type === FETCH_PRICES_FAILURE) {
    return { ...state, prices: null, error: action.error, fetchingProgress: false };
  }

  if (action.type === FETCHING_PRICES) {
    return { ...state, prices: null, fetchingProgress: true, error: null, address: action.address };
  }

  if (action.type === CLEAR_PRICE) {
    return { ...state, prices: null, fetchingProgress: false };
  }

  return state;
};
