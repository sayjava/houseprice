const POSTCODE_FAILURE = 'postcode_failure';
const POSTCODE_PROGRESS = 'postcode_progress';
const POSTCODE_SUCCESSFUL = 'postcode_successful';

const postcodeFailure = error => ({ type: POSTCODE_SUCCESSFUL, error });
const postcodeSuccessful = postcode => ({ type: POSTCODE_SUCCESSFUL, postcode });

const postcodeProgress = () => ({ type: POSTCODE_PROGRESS });

export const fetchPostcode = (address) => {
  const { places } = window.google.maps;
  const service = new places.PlacesService(document.createElement('div'));

  return dispatch =>
    new Promise((resolve, reject) => {
      dispatch(postcodeProgress());
      service.getDetails(address, (place, status) => {
        if (status === places.PlacesServiceStatus.OK) {
          const postcode = place.address_components.find(
            comp => comp.types.join('').indexOf('postal_code') > -1,
          );

          const thePostcode = {
            postcode,
            text: place.formatted_address,
            name: place.name,
            placeId: place.place_id,
            district: place.vicinity,
            geometry: place.geometry,
          };

          resolve(thePostcode);
          return dispatch(postcodeSuccessful(thePostcode));
        }
        const failed = {
          status,
          message: 'Place Error',
        };
        reject(failed);
        return dispatch(postcodeFailure(failed));
      });
    });
};

export default (state = { postcode: null }, action) => {
  if (action.type === POSTCODE_SUCCESSFUL) {
    return { ...state, postcode: action.postcode, error: null, progress: null };
  }

  if (action.type === POSTCODE_PROGRESS) {
    return { ...state, postcode: null, error: null, progress: true };
  }

  if (action.type === POSTCODE_FAILURE) {
    return { ...state, postcode: null, error: action.error, progress: null };
  }

  return state;
};
