import price, { clearPrice, fetchPrices } from './price';

jest.mock('lscache');
const lscache = require('lscache');

describe('Price', () => {
  describe('Reducing', () => {
    it('should create a new state from a successful action', () => {
      const oldState = {
        postcode: {
          name: 'basic-code',
        },
      };
      const action = {
        type: 'fetch_prices_successful',
        prices: ['price-1', 'price-2'],
        postcode: {
          name: 'saved-code',
        },
      };

      const newState = price(oldState, action);
      expect(newState).toEqual({
        postcode: {
          name: 'saved-code',
        },
        error: null,
        fetchingProgress: false,
        prices: ['price-1', 'price-2'],
      });
    });
    it('should produce an error state with error action', () => {
      const action = {
        type: 'fetch_prices_failure',
        error: {
          message: 'failed true',
        },
      };
      const oldState = {
        oldProps: [1, 2, 3],
      };

      const newState = price(oldState, action);

      expect(newState).toEqual({
        oldProps: [1, 2, 3],
        prices: null,
        fetchingProgress: false,
        error: {
          message: 'failed true',
        },
      });
    });
    it('should produce an fetching progress state', () => {
      const action = {
        type: 'fetching_prices',
        address: { name: 'fetching-address' },
      };
      const oldState = {
        oldProps: [1, 2, 3],
      };

      const newState = price(oldState, action);

      expect(newState).toEqual({
        oldProps: [1, 2, 3],
        address: { name: 'fetching-address' },
        prices: null,
        fetchingProgress: true,
        error: null,
      });
    });
    it('should  clear prices', () => {
      const oldState = {
        oldProps: [1, 2, 3],
      };

      const newState = price(oldState, clearPrice());

      expect(newState).toEqual({
        oldProps: [1, 2, 3],
        prices: null,
        fetchingProgress: false,
      });
    });
    it('should return a default state when no action matches', () => {
      const action = {
        type: 'unknown_action',
      };
      const oldState = {
        someOldProps: { values: [1, 2, 3] },
      };

      const newState = price(oldState, action);

      expect(newState).toEqual({
        someOldProps: { values: [1, 2, 3] },
      });
    });
  });

  describe('fetching price', () => {
    let dispatcher;
    beforeEach(() => {
      global.ga = jest.fn();
      global.fetch = jest.fn(() => Promise.reject({}));
      dispatcher = jest.fn(cb => cb);
    });

    it('should call fetch with address name', () => {
      fetchPrices({
        postcode: null,
        name: 'addy',
      })(dispatcher);

      expect(global.fetch).toHaveBeenLastCalledWith(
        'http://localhost:8010/houseprice-7da29/us-central1/district?district=addy',
      );
    });

    it('should call fetch with postcode', () => {
      fetchPrices({
        postcode: {
          short_name: 'SW1B34',
        },
      })(dispatcher);

      expect(global.fetch).toHaveBeenLastCalledWith(
        'http://localhost:8010/houseprice-7da29/us-central1/postcode?postcode=SW1B34',
      );
    });

    it('should call fetch with postcode', () => {
      fetchPrices({
        district: 'westminster',
      })(dispatcher);

      expect(global.fetch).toHaveBeenLastCalledWith(
        'http://localhost:8010/houseprice-7da29/us-central1/district?district=westminster',
      );
    });

    it('should dispatch a progress action', () => {
      fetchPrices({
        postcode: null,
        name: 'addy',
      })(dispatcher);

      expect(dispatcher).toHaveBeenLastCalledWith({
        address: { name: 'addy', postcode: null },
        type: 'fetching_prices',
      });
    });

    it('should dispatch a successful fetch action', () => {
      global.fetch = () =>
        Promise.resolve({
          json: () =>
            Promise.resolve([
              {
                avg: 239405,
                key: 'normandy',
              },
            ]),
        });

      return fetchPrices({
        postcode: null,
        name: 'addy',
      })(dispatcher).then(() => {
        expect(dispatcher).toHaveBeenLastCalledWith({
          postcode: { name: 'addy', postcode: null },
          prices: { normandy: { avg: 239405, key: 'normandy', price: '£239,405' } },
          type: 'fetch_prices_successful',
        });
      });
    });
  });

  describe('price cache', () => {
    let dispatcher;

    beforeEach(() => {
      global.fetch = jest.fn();
      dispatcher = jest.fn(cb => cb);
    });

    it('should call cached with fetch url', () => {
      fetchPrices({
        district: 'waterloo',
      });

      expect(lscache.get).lastCalledWith(
        'http://localhost:8010/houseprice-7da29/us-central1/district?district=addy',
      );
    });

    it('should return cached successful results', () => {
      lscache.get.mockImplementation(() => ({
        prices: ['price-1', 'price-2'],
      }));

      fetchPrices({
        district: 'waterloo',
      })(dispatcher);

      expect(dispatcher).lastCalledWith({
        postcode: { district: 'waterloo' },
        prices: { prices: ['price-1', 'price-2'] },
        type: 'fetch_prices_successful',
      });
    });

    it('should return cached failed results', () => {
      lscache.get.mockImplementation(() => ({}));

      return fetchPrices({
        district: 'waterloo',
      })(dispatcher).then(() => {
        expect(dispatcher).lastCalledWith({
          error: { message: 'Prices not found for undefined, please try another area or postcode' },
          type: 'fetch_prices_failure',
        });
      });
    });

    it('should catch an error and dispatch failure if network data is bad', () => {
      global.fetch = () =>
        Promise.resolve({
          json: () => Promise.reject(404),
        });

      return fetchPrices({
        postcode: null,
        name: 'addy',
      })(dispatcher).then(null, () => {
        expect(dispatcher).toHaveBeenLastCalledWith({
          error: { message: 'Prices not found for addy, please try another area or postcode' },
          type: 'fetch_prices_failure',
        });
      });
    });
  });
});
