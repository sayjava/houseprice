import address, { searchAddress, clearAddress, selectAddress } from './address';

const createPredictions = (result, status) => {
  global.google = {
    maps: {
      places: {
        AutocompleteService() {
          return {
            getPlacePredictions: (param, cb) => cb(result, status),
          };
        },
        PlacesServiceStatus: {
          OK: true,
        },
      },
    },
  };
};

describe('Address Reducer', () => {
  it('should generate a successful action when prediction succeeds', () => {
    const mockDispatcher = jest.fn(cb => cb);
    createPredictions([{ name: 'home1' }, { name: 'home2' }], true);

    return searchAddress('home-address')(mockDispatcher).then(() => {
      expect(mockDispatcher).toHaveBeenLastCalledWith({
        result: {
          addresses: [{ name: 'home1' }, { name: 'home2' }],
          text: 'home-address',
        },
        type: 'searched_addresses_success',
      });
    });
  });

  it('should generate a successful action when prediction succeeds', () => {
    const mockDispatcher = jest.fn(cb => cb);
    createPredictions([{ name: 'home1' }, { name: 'home2' }], false);

    return searchAddress('home-address')(mockDispatcher).then(null, () => {
      expect(mockDispatcher).toHaveBeenLastCalledWith({
        error: { message: 'Google Services Unavailable', status: false },
        type: 'searched_addresses_failure',
      });
    });
  });

  describe('Reducer', () => {
    it('should clear address stored', () => {
      const oldState = {
        selectedAddress: { name: 'Some old address' },
      };
      expect(address(oldState, clearAddress())).toEqual({ addresses: [], selectedAddress: null });
    });

    it('should select an address', () => {
      expect(address({}, selectAddress({ name: 'Home Address' }))).toEqual({
        selectedAddress: {
          name: 'Home Address',
        },
      });
    });
  });
});
