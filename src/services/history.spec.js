import history, { fillHistory, clearHistory, clearPriceHistory, newHistory } from './history';

jest.mock('lscache');

const lscache = require('lscache');

describe('History', () => {
  describe('Reducer', () => {
    beforeEach(() => {
      lscache.get.mockImplementation(() => ({}));
    });

    it('should fill the history', () => {
      const newState = history({}, fillHistory());
      const histories = Object.keys(newState.priceHistory);

      expect(histories).toEqual([
        'ChIJRwLA2qv4a0gR_Vicygy6xhI',
        'ChIJPeqVDlONbEgRk4X1zrUsKDs',
        'ChIJnUpjrRgEdkgRRrOuNiUu8o8',
        'ChIJNxJ0UPQPdkgRXik3cFTbw-0',
      ]);
    });

    it('should cear all price history', () => {
      const firstState = history({}, fillHistory());
      const newState = history(firstState, clearHistory());

      expect(newState.priceHistory).toEqual({});
    });

    it('should cear a price history', () => {
      lscache.get.mockImplementation(() => ({
        ChIJRwLA2qv4a0gR_Vicygy6xhI: {},
        ChIJPeqVDlONbEgRk4X1zrUsKDs: {},
        ChIJnUpjrRgEdkgRRrOuNiUu8o8: {},
        'ChIJNxJ0UPQPdkgRXik3cFTbw-0': {},
      }));
      const firstState = history({}, fillHistory());
      const newState = history(firstState, clearPriceHistory('ChIJRwLA2qv4a0gR_Vicygy6xhI'));
      const histories = Object.keys(newState.priceHistory);

      expect(histories).toEqual([
        'ChIJPeqVDlONbEgRk4X1zrUsKDs',
        'ChIJnUpjrRgEdkgRRrOuNiUu8o8',
        'ChIJNxJ0UPQPdkgRXik3cFTbw-0',
      ]);
    });

    it('should store new price history', () => {
      lscache.get.mockImplementation(() => ({
        anOldPlaceId: {
          name: 'OldPlace',
        },
      }));
      const newState = history(
        {},
        newHistory({
          address: {
            placeId: 'aNewPlaceId',
            name: 'New-Place',
          },
          prices: {
            TD: {},
            TY: {},
          },
        }),
      );

      expect(newState.priceHistory).toEqual({
        anOldPlaceId: {
          name: 'OldPlace',
        },
        aNewPlaceId: {
          address: {
            name: 'New-Place',
            placeId: 'aNewPlaceId',
          },
          prices: {
            TD: {},
            TY: {},
          },
        },
      });

      //   expect(lscache.set).toHaveBeenLastCalledWith({});
    });
  });
});
