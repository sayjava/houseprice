const SELECT_ADDRESS = 'select_address';
const SEARCHED_ADDRESSES_SUCCESS = 'searched_addresses_success';
const SEARCHED_ADDRESSES_FAILURE = 'searched_addresses_failure';
const CLEAR_ADDRESS = 'clear_address';

export const selectAddress = address => ({ type: SELECT_ADDRESS, address });

export const searchedAddressesSuccess = ({ text, addresses }) => ({
  type: SEARCHED_ADDRESSES_SUCCESS,
  result: { text, addresses },
});

export const searchedAddressesFailure = error => ({ type: SEARCHED_ADDRESSES_FAILURE, error });

export const clearAddress = () => ({ type: CLEAR_ADDRESS });

export const searchAddress = (input) => {
  const { places } = window.google.maps;
  const autoCompleteService = new places.AutocompleteService();

  return dispatch =>
    new Promise((resolve, reject) => {
      autoCompleteService.getPlacePredictions(
        {
          input,
          componentRestrictions: { country: 'UK' },
        },
        (predictions, status) => {
          if (status === places.PlacesServiceStatus.OK) {
            return resolve(
              dispatch(searchedAddressesSuccess({ text: input, addresses: predictions })),
            );
          }
          return reject(
            dispatch(
              searchedAddressesFailure({
                message: 'Google Services Unavailable',
                status,
              }),
            ),
          );
        },
      );
    });
};

export default (state = { selectedAddress: null }, action) => {
  if (action.type === SELECT_ADDRESS) {
    return { ...state, selectedAddress: action.address };
  }

  if (action.type === SEARCHED_ADDRESSES_SUCCESS) {
    return { ...state, ...action.result };
  }

  if (action.type === SEARCHED_ADDRESSES_FAILURE) {
    return { ...state, error: action.error };
  }

  if (action.type === CLEAR_ADDRESS) {
    return { ...state, selectedAddress: null, addresses: [] };
  }

  return state;
};
