import { fetchPostcode } from './postcode';

const initPlaceService = (response, status) => {
  global.google = {
    maps: {
      places: {
        PlacesServiceStatus: {
          OK: true,
        },
        PlacesService: class {
          getDetails(address, callback) {
            return callback(response, status);
          }
        },
      },
    },
  };
};

describe('Postcode', () => {
  describe('Successful Fetch', () => {
    it('should dispatch a progress action', () => {
      initPlaceService({}, false);
      const mockDispatcher = jest.fn();

      return fetchPostcode({ name: 'whereILive' })(mockDispatcher).then(null, () => {
        expect(mockDispatcher).toHaveBeenCalledWith({ type: 'postcode_progress' });
      });
    });

    it('should dispatch a successful action', () => {
      const result = {
        address_components: [
          {
            types: ['postal_code'],
          },
        ],
      };
      initPlaceService(result, true);
      const mockDispatcher = jest.fn();

      return fetchPostcode({ name: 'whereILive' })(mockDispatcher).then(() => {
        expect(mockDispatcher).toHaveBeenCalledWith({
          postcode: {
            district: undefined,
            geometry: undefined,
            name: undefined,
            placeId: undefined,
            postcode: { types: ['postal_code'] },
          },
          type: 'postcode_successful',
        });
      });
    });
  });
});
