import lscache from 'lscache';
import localCache from '../cache';

const NEW_PRICE_HISTORY = 'new_price_history';
const INIT_HISTORY = 'fill_history';
const CLEAR_HISTORY = 'clear_history';
const CLEAR_PRICE_HISTORY = 'clear_price_history';
const NEW_BUILD = 'new_builds';

const HISTORY_KEY = 'SEARCH_HISTORY';
const CACHE_VERSION = 3;
const CACHE_VERSION_KEY = 'cache_key';
const ONE_MONTH = 34560;

const initHistory = () => {
  // check cache version
  if (Number(lscache.get(CACHE_VERSION_KEY) || 0) < CACHE_VERSION) {
    lscache.flush();
    lscache.set(HISTORY_KEY, {});
    lscache.set(CACHE_VERSION_KEY, CACHE_VERSION);
  } else if (!lscache.get(HISTORY_KEY)) {
    lscache.set(HISTORY_KEY, {});
  }
};

export const fillHistory = () => ({ type: INIT_HISTORY });

export const newHistory = newPriceResult => ({ type: NEW_PRICE_HISTORY, newPriceResult });

export const clearHistory = () => ({ type: CLEAR_HISTORY });

export const clearPriceHistory = priceId => ({ type: CLEAR_PRICE_HISTORY, priceId });

export const toggleNewBuild = newBuilds => ({ type: NEW_BUILD, newBuilds });

export default (state = { priceHistory: {}, selectedType: 'FN', newBuilds: false }, action) => {
  if (action.type === NEW_PRICE_HISTORY) {
    const priceHistory = lscache.get(HISTORY_KEY);
    const { prices, address } = action.newPriceResult;

    if (prices && Object.keys(prices).length) {
      priceHistory[address.placeId] = {
        address,
        prices,
      };
      lscache.set(HISTORY_KEY, priceHistory, ONE_MONTH);
    }
    return { ...state, priceHistory };
  }

  if (action.type === INIT_HISTORY) {
    if (Object.keys(lscache.get(HISTORY_KEY)).length === 0) {
      lscache.set(HISTORY_KEY, localCache, ONE_MONTH);
      return { ...state, priceHistory: localCache };
    }
    return { ...state, priceHistory: lscache.get(HISTORY_KEY) };
  }

  if (action.type === CLEAR_HISTORY) {
    lscache.set(HISTORY_KEY, {});
    return { ...state, priceHistory: {} };
  }

  if (action.type === CLEAR_PRICE_HISTORY) {
    const prices = lscache.get(HISTORY_KEY);
    delete prices[action.priceId];
    lscache.set(HISTORY_KEY, prices);
    return { ...state, priceHistory: prices };
  }

  return state;
};

initHistory();
