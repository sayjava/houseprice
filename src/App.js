import React from 'react';

import MapView from './components/Map/Map';
import AddressContent from './components/Input/AddressContent';
import PriceContent from './components/Price/PriceContent';
import CompareTable from './components/Compare/CompareTable';

import './App.css';

export default () => (
  <div className="app">
    <div className="app-header">
      <i className="fa fa-2x fa-home app-header__app-icon" aria-hidden="true" />
      <div className="app-header__search">
        <AddressContent />
      </div>
      <h5 className="app-header__text-lead">Compare UK Average House Prices</h5>
    </div>

    <div className="app-content">
      <MapView />
      <PriceContent />
      <CompareTable />
    </div>

    <div className="app-footer">
      <span>
        Price data is sourced from the
        <a
          className="app-footer__link"
          href="https://www.gov.uk/government/organisations/land-registry"
        >
          HM Land Registry Database
        </a>
      </span>
    </div>
  </div>
);
