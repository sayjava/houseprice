const csvToTransactions = function (csv) {

    return csv.split('\n').reduce((acc, line) => {
        let [id,
            price,
            date,
            postcode,
            type,
            newBuild,
            estateType,
            building,
            town,
            district,
            county,
            borough
        ] = line.split(',')

        if (type) {

            type = type.replace(/\"/g, '')
            newBuild = newBuild.replace(/\"/g, '')

            let transaction = {
                id,
                price: parseFloat(price.replace(/\"/g, '')),
                date: date.replace(/\"/g, ''),
                postcode: postcode.replace(/\"/g, ''),
                type,
                newBuild: newBuild.replace(/\"/g, ''),
                estateType: estateType.replace(/\"/g, ''),
                building: building.replace(/\"/g, ''),
                town: town.replace(/\"/g, ''),
                county: county.replace(/\"/g, '')
            }

            // basic types including new builds
            acc[type] = acc[type] || []
            acc[type].push(transaction)

            // types of new builds
            acc[type + newBuild] = acc[type + newBuild] || []
            acc[type + newBuild].push(transaction)
        }

        return acc
    }, {})
}

let calculateAvgerages = function (transactions) {

    return Object.keys(transactions).map((key) => {
        let typeTransactions = transactions[key]
        let avg = typeTransactions.reduce((acc, transac) => {
            return acc + Math.floor(transac.price / typeTransactions.length)
        }, 0)
        return {
            key,
            avg
        }
    })

}

let calculateGrowth = function (transactions) {

    let thisYear = (new Date()).getFullYear()
    let lastYear = Number(thisYear) - 1

    let average = (values) => values.reduce((acc, transac) => {
        return acc + Math.floor(transac.price / values.length)
    }, 0)

    return Object.keys(transactions).map((key) => {
        let typeTransactions = transactions[key]

        let lastYearPrices = typeTransactions.filter((transac) => {
            return transac.date.indexOf(lastYear) > -1
        })

        let thisYearPrices = typeTransactions.filter((transac) => {
            return transac.date.indexOf(thisYear) > -1
        })

        let thisYearAvg = average(thisYearPrices)
        let lastYearAvg = average(lastYearPrices)

        // if there are less than 10 transation in the current year, use the combo of last year and this year averages
        let avg = thisYearPrices.length > 10 ? thisYearAvg : (thisYearAvg + lastYearAvg) / 2

        return {
            key,
            avg,
            thisYearCount: thisYearPrices.length,
            lastYearCount: lastYearPrices.length,
            thisYearAvg,
            lastYearAvg,
            growth: (1 - (lastYearAvg / thisYearAvg)) * 100
        }
    })
}

module.exports.calculateGrowth = calculateGrowth
module.exports.averages = calculateAvgerages
module.exports.transactions = csvToTransactions
