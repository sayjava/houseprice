const cachedData = require('fs')
  .readFileSync('./data/ppd_data.csv')
  .toString();
const csv = require('./csv');

module.exports.byCached = () =>
  new Promise((resolve) => {
    const transactions = csv.transactions(cachedData);
    const growth = csv.calculateGrowth(transactions);
    resolve(growth);
  });
