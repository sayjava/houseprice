const responseData = require('./response').responseData;
const csv = require('./csv');
const http = require('http');

const year = new Date().getFullYear() - 1;

const byTown = (town) => {
  const uri =
    'http://landregistry.data.gov.uk/app/ppd/ppd_data.csv?et%5B%5D=lrcommon%3Afreehold&et%5B%5D=lrcommon%3Aleasehold&' +
    `limit=all&min_date=1+January+${year}&nb%5B%5D=true&nb%5B%5D=false&ptype%5B%5D=lrcommon%3Adetached` +
    '&ptype%5B%5D=lrcommon%3Asemi-detached&ptype%5B%5D=lrcommon%3Aterraced&ptype%5B%5D=lrcommon%3Aflat-maisonett' +
    'e&ptype%5B%5D=lrcommon%3AotherPropertyType&tc%5B%5D=ppd%3AstandardPricePaidTransaction' +
    `&tc%5B%5D=ppd%3AadditionalPricePaidTransaction&town=${town}`;

  return new Promise((resolve, reject) => {
    http.get(uri, (res) => {
      responseData(res).then((text) => {
        const transactions = csv.transactions(text);
        resolve(transactions);
      }, reject);
    });
  });
};

const byCounty = (county) => {
  const uri =
    'http://landregistry.data.gov.uk/app/ppd/ppd_data.csv?et%5B%5D=lrcommon%3Afreehold&et%5B%5D=lrcommon%3Aleasehold&' +
    `limit=all&min_date=1+January+${year}&nb%5B%5D=true&nb%5B%5D=false&ptype%5B%5D=lrcommon%3Adetached` +
    '&ptype%5B%5D=lrcommon%3Asemi-detached&ptype%5B%5D=lrcommon%3Aterraced&ptype%5B%5D=lrcommon%3Aflat-maisonett' +
    'e&ptype%5B%5D=lrcommon%3AotherPropertyType&tc%5B%5D=ppd%3AstandardPricePaidTransaction' +
    `&tc%5B%5D=ppd%3AadditionalPricePaidTransaction&county=${county}`;

  return new Promise((resolve, reject) => {
    http.get(uri, (res) => {
      responseData(res).then((text) => {
        const transactions = csv.transactions(text);
        resolve(transactions);
      }, reject);
    });
  });
};

module.exports.byDistrict = function (district) {
  const uri =
    `http://landregistry.data.gov.uk/app/ppd/ppd_data.csv?district=${district}` +
    '&et%5B%5D=lrcommon%3Afreehold&et%5B%5D=lrcommon%3Aleasehold&limit=all' +
    `&min_date=1+January+${year}&nb%5B%5D=true&nb%5B%5D=false&ptype%5B%5D` +
    '=lrcommon%3Adetached&ptype%5B%5D=lrcommon%3Asemi-detached&ptype%5B%5D=lrcommon' +
    '%3Aterraced&ptype%5B%5D=lrcommon%3Aflat-maisonette&ptype%5B%5D=lrcommon%3' +
    'AotherPropertyType&tc%5B%5D=ppd%3AstandardPricePaidTransaction&tc%5B%5D=ppd%3AadditionalPricePaidTransaction';

  return new Promise((resolve, reject) => {
    http.get(uri, (res) => {
      // by district
      responseData(res).then((text) => {
        const districts = csv.transactions(text);

        if (Object.keys(districts).length > 0) {
          resolve(csv.calculateGrowth(districts));
        } else {
          // by town
          byTown(district).then((towns) => {
            if (Object.keys(towns).length > 0) {
              resolve(csv.calculateGrowth(towns));
            } else {
              byCounty(district).then((counties) => {
                resolve(csv.calculateGrowth(counties));
              });
            }
          });
        }
      }, reject);
    });
  });
};
