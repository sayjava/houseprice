const functions = require('firebase-functions');
const cors = require('cors')({ origin: true });

const byPostcode = require('./pd/postcode').byPostcode;
const byDistrict = require('./pd/district').byDistrict;

const CACHE_CONTROL = 'public, max-age=60000, s-maxage=60000';

exports.postcode = functions.https.onRequest((req, res) => {
  res.set('Cache-Control', CACHE_CONTROL);
  const { postcode } = req.query;
  cors(req, res, () => {
    byPostcode(postcode).then(data => res.status(200).send(data), err => res.status(400).send(err));
  });
});

exports.district = functions.https.onRequest((req, res) => {
  res.set('Cache-Control', CACHE_CONTROL);
  const { district } = req.query;
  cors(req, res, () => {
    byDistrict(district).then(data => res.status(200).send(data), err => res.status(400).send(err));
  });
});
